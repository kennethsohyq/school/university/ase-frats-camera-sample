#! /bin/sh

echo "Replacing Files"
cd lib
sed -i "s+import 'package:ase_camera_test/webcam.dart';+//import 'package:ase_camera_test/webcam.dart';+g" main.dart
sed -i 's+Navigator.push(context, MaterialPageRoute(builder: (context) => WebCamApp()),);+//Navigator.push(context, MaterialPageRoute(builder: (context) => WebCamApp()),);+g' main.dart
cd ..
echo "Files Replaced"