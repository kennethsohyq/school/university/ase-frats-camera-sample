import 'dart:html';
import 'dart:convert';
import 'dart:ui' as ui;
import 'package:ase_camera_test/util.dart';
import 'package:flutter/material.dart';

class WebCamApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: WebcamPage());
  }
}

class WebcamPage extends StatefulWidget {
  @override
  _WebcamPageState createState() => _WebcamPageState();
}

class _WebcamPageState extends State<WebcamPage> {
  // Webcam widget to insert into the tree
  Widget _webcamWidget;

  // VideoElement
  VideoElement _webcamVideoElement;

  @override
  void dispose() {
    super.dispose();
    print("Disposing");
    _webcamVideoElement.srcObject.getTracks().forEach((element) {
      element.stop();
    });
    _webcamVideoElement.pause();
    _webcamVideoElement.srcObject = null;
    //_webcamVideoElement.src = "";
    _webcamVideoElement.load();
    //_webcamVideoElement = null;
  }

  @override
  void initState() {
    super.initState();
    print("Init");
    // Create an video element which will be provided with stream source
    if (_webcamVideoElement == null)
      _webcamVideoElement = VideoElement()..autoplay = true;
    // Register an webcam
    // ignore: undefined_prefixed_name
    ui.platformViewRegistry.registerViewFactory(
        'webcamVideoElement', (int viewId) => _webcamVideoElement);
    // Create video widget
    if (_webcamWidget == null)
      _webcamWidget =
          HtmlElementView(key: UniqueKey(), viewType: 'webcamVideoElement');
    // Access the webcam stream
    window.navigator.getUserMedia(video: true).then((MediaStream stream) {
      print("Video obtained. Assigning Stream");
      _webcamVideoElement.srcObject = stream;
      _webcamVideoElement.onLoadedMetadata.capture((event) {
        _webcamVideoElement.play();
      });
    });
  }

  void showInSnackBar(String message, BuildContext context) {
    Scaffold.of(context).showSnackBar(SnackBar(content: Text(message)));
  }

  void _takePicture(BuildContext context) {
    showInSnackBar("Taking photo and matching face...", context);
    print("Taking Picture now");

    CanvasElement temp = CanvasElement();
    temp.width = _webcamVideoElement.videoWidth;
    temp.height = _webcamVideoElement.videoHeight;
    CanvasRenderingContext2D tempContent = temp.getContext("2d");
    tempContent.drawImage(_webcamVideoElement, 0, 0);
    String dataUrl = temp.toDataUrl("image/png");
    print("Image: $dataUrl");

    print(
        "DEBUG: Video Width: ${_webcamVideoElement.videoWidth} | Video Height: ${_webcamVideoElement.videoHeight}");
    print(
        "DEBUG: Canvas Width: ${_webcamVideoElement.offsetWidth} | Canvas Height: ${_webcamVideoElement.offsetHeight}");

    AWSUtil.queryAWS(dataUrl, _faceDetails).then((gotFace) {
      if (gotFace == null) {
        print("No Face Found!");
        showInSnackBar("Person not found in system", context);
        return;
      }
      if (!_faceDetails)
        _processFaceRecog(gotFace, tempContent, temp);
      else
        _processFaceDetect(gotFace, tempContent, temp);
    });
  }

  void _processFaceDetect(Map<String, dynamic> data,
      CanvasRenderingContext2D canvasCtx, CanvasElement canvas) {
    print("Processing Face");
    var boundingBox = data["BoundingBox"];
    canvasCtx.rect(
        boundingBox["Left"] * canvas.width,
        boundingBox["Top"] * canvas.height,
        boundingBox["Width"] * canvas.width,
        boundingBox["Height"] * canvas.height);
    canvasCtx.lineWidth = 4;
    canvasCtx.strokeStyle = "red";
    canvasCtx.stroke();

    print("Displaying on dialog");
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Face Details"),
            content: SingleChildScrollView(
              child: ListBody(children: <Widget>[
                Image.memory(base64Decode(canvas.toDataUrl().substring(22)),
                    width: canvas.width.toDouble(),
                    height: canvas.height.toDouble(),
                    fit: BoxFit.fill),
                Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Text(AWSUtil.generateAwsFaceDetailsData(data)),
                )
              ]),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _processFaceRecog(Map<String, dynamic> gotFace,
      CanvasRenderingContext2D tempContent, CanvasElement temp) {
    print("Face ID: ${gotFace["face"]}, matching user");

    if (gotFace["uuid"] != null) {
      if (AWSUtil.faceDataInAws.containsKey(gotFace["uuid"])) {
        String person = AWSUtil.faceDataInAws[gotFace["uuid"]];
        print("Found $person");
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("Found $person (${gotFace["uuid"]}) from app itself. Please migrate user to DB instead")));
      } else {
        print("AWS Found person but person not in app ($gotFace)");
        _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text("AWS Found user but not in app. ID: $gotFace")));
      }
      return;
    }

    String person = gotFace["student"]["Name"];
    // Draw bounding box
    var boundingbox = gotFace["boundingBox"];
    tempContent.beginPath();
    tempContent.rect(
        boundingbox["Left"] * temp.width,
        boundingbox["Top"] * temp.height,
        boundingbox["Width"] * temp.width,
        boundingbox["Height"] * temp.height);
    tempContent.lineWidth = 4;
    tempContent.strokeStyle = "red";
    tempContent.stroke();

    // Draw name in bounding box
    tempContent.font = "30px Comic Sans MS";
    tempContent.fillStyle = "red";
    tempContent.fillText(person, (boundingbox["Left"] * temp.width) + 10,
        (boundingbox["Top"] * temp.height) + 30);

    print("Found $person");
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text("Found someone"),
            content: SingleChildScrollView(
              child: ListBody(children: <Widget>[
                Image.memory(
                  base64Decode(temp.toDataUrl().substring(22)),
                  // Remove data:image/png,base64,
                  width: temp.width.toDouble(),
                  height: temp.height.toDouble(),
                  fit: BoxFit.fill,
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5),
                  child: Text(
                      "Name: $person\nMatric Number: ${gotFace["student"]["MatricID"]}\nEmail: ${gotFace["student"]["Email"]}\nConfidence Rating: ${gotFace["confidence"]}"),
                ),
              ]),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Close'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _faceDetails = false;

  @override
  Widget build(BuildContext context) => Scaffold(
        key: _scaffoldKey,
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Smile at the camera to take attendance:',
                style: TextStyle(fontSize: 30, fontStyle: FontStyle.italic),
              ),
              Text(
                  "Note: If you do not see the camera stream, refresh the page. At least until this is fixed :("),
              Container(width: 1280, height: 720, child: _webcamWidget),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Toggle Face Details Mode"),
                  Switch(
                      value: _faceDetails,
                      onChanged: (bool value) {
                        setState(() {
                          _faceDetails = value;
                        });
                      })
                ],
              )
            ],
          ),
        ),
        floatingActionButton: Builder(
          builder: (context) => FloatingActionButton(
            onPressed: () {
              _takePicture(context);
            },
            tooltip: 'Take Picture',
            child: Icon(Icons.camera_alt),
          ),
        ),
      );
}
