import 'dart:convert';
import 'package:http/http.dart' as http;

class AWSUtil {

  static const api = "https://3hgs67x5sh.execute-api.ap-southeast-1.amazonaws.com/api/detectStudent";
  static const apiFace = "https://3hgs67x5sh.execute-api.ap-southeast-1.amazonaws.com/api/detectFace";
  //static const api = "http://localhost:3000/detect";

  // TODO: To Update Manually for now until we have a DB
  static const faceDataInAws = {
    "ad9dc261-1f6b-420e-9c40-9c2265669209": "Chua Wen Qing",
    "6b33403b-76ba-46cc-a04b-f3723f97e48c": "Lee Jian En",
    "da3a3e49-eda2-4b24-b241-72c2da76a741": "Ahmad Jazli"
  };

  static Future<Map<String, dynamic>> queryAWS(String photoData, [bool faceDetailsMode = false]) async {
    print("Querying AWS for Face");
    var response = await http.post((faceDetailsMode) ? apiFace : api, body: {'photo': photoData});
    print("Response Status: ${response.statusCode}");
    print("Response Body: ${response.body}");

    Map result = jsonDecode(response.body);
    if (!result.containsKey("found") || !result.containsKey("result") || !result["found"]) {
      if (!faceDetailsMode && result["result"].toString().startsWith("Found (")) {
        print("Returning UUID");
        String parser = result["result"].toString();
        RegExp exp = new RegExp('[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}');
        RegExpMatch match = exp.firstMatch(parser);
        print(match.group(0));
        return {
          "uuid": match.group(0)
        };
      }
      print("Not Found");
      return null;
    }

    if (!faceDetailsMode && !result["result"]["face"].containsKey("Face")) return null;

    if (!faceDetailsMode) {
      var faceResult = result["result"];
      if (!faceResult["face"]["Face"].containsKey("FaceId")) {
        print("Cannot find face id");
        return null;
      }
      return {
        "student": faceResult["student"],
        "face": faceResult["face"]["Face"]["FaceId"],
        "confidence": faceResult["face"]["Similarity"],
        "boundingBox": faceResult["face"]["SearchedFace"]
      };
    } else {
      return result["result"];
    }
  }

  static String generateAwsFaceDetailsData(Map<String, dynamic> data) {
    String s = "Information:\n";
    s += "Age Range: ${data["AgeRange"]["Low"]}-${data["AgeRange"]["High"]}\n";
    s += "Gender: ${data["Gender"]["Value"]} (${_roundedConfidence(data["Gender"]["Confidence"])})\n";
    s += "Is Smiling: ${_getYesNo(data["Smile"]["Value"])} (${_roundedConfidence(data["Smile"]["Confidence"])})\n";
    s += "Wearing Spectacles: ${_getYesNo(data["Eyeglasses"]["Value"])} (${_roundedConfidence(data["Eyeglasses"]["Confidence"])})\n";
    s += "Wearing Sunglasses: ${_getYesNo(data["Sunglasses"]["Value"])} (${_roundedConfidence(data["Sunglasses"]["Confidence"])})\n";
    s += "Has Beard: ${_getYesNo(data["Beard"]["Value"])} (${_roundedConfidence(data["Beard"]["Confidence"])})\n";
    s += "Has Mustache: ${_getYesNo(data["Mustache"]["Value"])} (${_roundedConfidence(data["Mustache"]["Confidence"])})\n";
    s += "Are Eyes Open: ${_getYesNo(data["EyesOpen"]["Value"])} (${_roundedConfidence(data["EyesOpen"]["Confidence"])})\n";
    s += "Is Mouth Opened: ${_getYesNo(data["MouthOpen"]["Value"])} (${_roundedConfidence(data["MouthOpen"]["Confidence"])})\n";
    s += "\nEmotions:\n";
    for (Map<String, dynamic> d in data["Emotions"]) {
      s += "Is ${d["Type"]}: ${(d["Confidence"] > 75) ? "Yes" : "No"} (${_roundedConfidence(d["Confidence"])})\n";
    }
    return s;
  }

  static String _roundedConfidence(double confidence) {
    return "${confidence.toStringAsFixed(2)}%";
  }

  static String _getYesNo(bool val) {
    return (val) ? "Yes" : "No";
  }
}