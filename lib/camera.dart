import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui' as ui;
import 'package:ase_camera_test/util.dart';
import 'package:flutter/material.dart';
import 'package:camera/camera.dart';
import 'package:path_provider/path_provider.dart';

List<CameraDescription> cameras;

Future<void> init() async {
  cameras = await availableCameras();
}

class CameraApp extends StatefulWidget {
  /// Returns a suitable camera icon for [direction].
  IconData getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.camera_rear;
      case CameraLensDirection.front:
        return Icons.camera_front;
      case CameraLensDirection.external:
        return Icons.camera;
    }
    throw ArgumentError('Unknown lens direction');
  }

  @override
  _CameraAppState createState() => _CameraAppState();
}

class _CameraAppState extends State<CameraApp> {
  CameraController controller;

  static const appendBase6Jpg = "data:image/jpeg;base64,";

  @override
  void initState() {
    super.initState();
    var valres = init();
    valres.then((value) {
      controller = CameraController(cameras[0], ResolutionPreset.medium);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {});
      });
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  bool _faceDetectMode = false;

  @override
  Widget build(BuildContext context) {
    if (controller == null || !controller.value.isInitialized) {
      return Container();
    }
    return Scaffold(
      key: _scaffoldKey,
      body: Column(
        children: <Widget>[
          Expanded(
            child: Container(
              padding: const EdgeInsets.all(1.0),
              child: Center(
                child: _cameraWidget(),
              ),
            ),
          ),
          _captureControlRowWidget(),
          Padding(
            padding: const EdgeInsets.all(5.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                _cameraTogglesRowWidget(),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _cameraWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return const Text(
        'Tap a camera',
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
          fontWeight: FontWeight.w900,
        ),
      );
    } else {
      return AspectRatio(
        aspectRatio: controller.value.aspectRatio,
        child: CameraPreview(controller),
      );
    }
  }

  /// Display the control bar with buttons to take pictures and record videos.
  Widget _captureControlRowWidget() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        IconButton(
          icon: const Icon(Icons.camera_alt),
          color: Colors.blue,
          onPressed: controller != null &&
                  controller.value.isInitialized &&
                  !controller.value.isRecordingVideo
              ? onTakePictureButtonPressed
              : null,
        ),
      ],
    );
  }

  void showInSnackBar(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(content: Text(message)));
  }

  void showToast(String message) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(message),
      duration: Duration(milliseconds: 1000),
    ));
  }

  void onNewCameraSelected(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }
    controller = CameraController(
      cameraDescription,
      ResolutionPreset.medium,
      enableAudio: false,
    );

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) setState(() {});
      if (controller.value.hasError) {
        showInSnackBar('Camera error ${controller.value.errorDescription}');
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

  void _showCameraException(CameraException e) {
    print('Error: ${e.code}\nError Message: ${e.description}');
    showInSnackBar('Error: ${e.code}\n${e.description}');
  }

  String imagePath;

  Future<String> convertImageToBase64(String filePath) async {
    File image = new File(filePath);
    List<int> imageB = await image.readAsBytes();
    return "$appendBase6Jpg${base64Encode(imageB)}";
  }

  void onTakePictureButtonPressed() {
    takePicture().then((String filePath) {
      if (mounted) {
        setState(() {
          imagePath = filePath;
          Future<String> base64File = convertImageToBase64(filePath);
          base64File.then((base64Data) {
            print("Image Base64: $base64Data");
            AWSUtil.queryAWS(base64Data, _faceDetectMode).then((gotFace) {
              if (gotFace == null) {
                print("No Face Found!");
                showInSnackBar("Person not found in system");
                return;
              }
              if (!_faceDetectMode)
                _processFaceRecog(gotFace, base64Data);
              else
                _processFaceDetect(gotFace, base64Data);
            });
          });
        });
        if (filePath != null) {
          print('Picture saved to $filePath');
          showToast("Matching Face with AWS...");
        }
      }
    });
  }

  void _processFaceDetect(Map<String, dynamic> data, String base64Image) {
    print("Processing Face");
    var boundingBox = data["BoundingBox"];
    ui
        .instantiateImageCodec(base64Decode(base64Image.substring(23)))
        .then((codec) {
      codec.getNextFrame().then((frameInfo) {
        var image = frameInfo.image;
        print("Displaying dialog...");
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("Showing Facial Information"),
                content: SingleChildScrollView(
                  child: ListBody(
                    children: <Widget>[
                      _drawFaceBox(image, boundingBox),
                      Padding(
                        padding: EdgeInsets.only(top: 5),
                        child: Text(AWSUtil.generateAwsFaceDetailsData(data)),
                      )
                    ],
                  ),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      });
    });
  }

  void _processFaceRecog(Map<String, dynamic> gotFace, String base64Data) {
    print("Face ID: ${gotFace["face"]}, matching user");

    if (gotFace["uuid"] != null) {
      if (AWSUtil.faceDataInAws.containsKey(gotFace["uuid"])) {
        String person = AWSUtil.faceDataInAws[gotFace["uuid"]];
        print("Found $person");
        showInSnackBar(
            "Found $person (${gotFace["uuid"]}) from app itself. Please migrate user to DB instead}");
      } else {
        print("AWS Found person but person not in app ($gotFace)");
        showInSnackBar("AWS Found user but not in app. ID: $gotFace");
      }
      return;
    }

    String person = gotFace["student"]["Name"];
    ui
        .instantiateImageCodec(base64Decode(base64Data.substring(23)))
        .then((codec) {
      codec.getNextFrame().then((frameInfo) {
        print("Return Image");
        var image = frameInfo.image;
        print("Image prepared. Showing Dialog Now");
        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                title: Text("Student Found"),
                content: SingleChildScrollView(
                  child: ListBody(children: <Widget>[
                    _buildImage(image, gotFace, person),
                    Padding(
                      padding: EdgeInsets.only(top: 5),
                      child: Text(
                          "Name: $person\nMatric Number: ${gotFace["student"]["MatricID"]}\nEmail: ${gotFace["student"]["Email"]}\nConfidence Rating: ${gotFace["confidence"]}"),
                    ),
                  ]),
                ),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Close'),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            });
      });
    });

    /*if (AWSUtil.faceDataInAws.containsKey(gotFace["face"])) {
      String person = AWSUtil.faceDataInAws[gotFace["face"]];
      print("Found $person");
      */ /*showInSnackBar(
          "Found $person (${gotFace["face"]})\nConfidence Rating: ${gotFace["confidence"]}");*/ /*
    } else {
      print("AWS Found person but person not in app DB ($gotFace)");
      showInSnackBar("AWS Found user but not in appdb. ID: $gotFace");
    }*/
  }

  Widget _drawFaceBox(ui.Image image, Map boundingBox) {
    return Container(
      child: CustomPaint(
        size: Size(image.width.toDouble() / 2, image.height.toDouble() / 2),
        painter: PersonWithImage(
            name: null,
            image: image,
            xRatio: boundingBox["Left"],
            yRatio: boundingBox["Top"],
            widthRatio: boundingBox["Width"],
            heightRatio: boundingBox["Height"]),
      ),
    );
  }

  Widget _buildImage(ui.Image image, Map faceData, String name) {
    print("Building Image");
    Map boundingBox = faceData["boundingBox"];
    return Container(
      child: CustomPaint(
        size: Size(image.width.toDouble() / 2, image.height.toDouble() / 2),
        painter: PersonWithImage(
            name: name,
            image: image,
            xRatio: boundingBox["Left"],
            yRatio: boundingBox["Top"],
            widthRatio: boundingBox["Width"],
            heightRatio: boundingBox["Height"]),
      ),
    );
  }

  Widget _cameraTogglesRowWidget() {
    final List<Widget> toggles = <Widget>[];

    if (cameras.isEmpty) {
      return const Text('No camera found');
    } else {
      for (CameraDescription cameraDescription in cameras) {
        toggles.add(
          SizedBox(
            width: 90.0,
            child: RadioListTile<CameraDescription>(
              title: Icon(getCameraLensIcon(cameraDescription.lensDirection)),
              groupValue: controller?.description,
              value: cameraDescription,
              onChanged: controller != null && controller.value.isRecordingVideo
                  ? null
                  : onNewCameraSelected,
            ),
          ),
        );
      }
      toggles.add(Padding(
          padding: EdgeInsets.only(left: 20),
          child: SizedBox(width: 41, child: Text("Face Detect"))));
      toggles.add(Switch(
          value: _faceDetectMode,
          onChanged: (bool newVal) {
            setState(() {
              _faceDetectMode = newVal;
            });
          }));
    }

    return Row(children: toggles);
  }

  /// Returns a suitable camera icon for [direction].
  IconData getCameraLensIcon(CameraLensDirection direction) {
    switch (direction) {
      case CameraLensDirection.back:
        return Icons.camera_rear;
      case CameraLensDirection.front:
        return Icons.camera_front;
      case CameraLensDirection.external:
        return Icons.camera;
    }
    throw ArgumentError('Unknown lens direction');
  }

  Future<String> takePicture() async {
    if (!controller.value.isInitialized) {
      showInSnackBar('Error: select a camera first.');
      return null;
    }
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Pictures/flutter_test';
    await Directory(dirPath).create(recursive: true);
    final String filePath = '$dirPath/${timestamp()}.jpg';

    if (controller.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      await controller.takePicture(filePath);
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
    return filePath;
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
}

class PersonWithImage extends CustomPainter {
  String name;
  ui.Image image;
  double xRatio, yRatio, widthRatio, heightRatio;
  double x, y, width, height;

  PersonWithImage(
      {this.name,
      this.image,
      this.xRatio,
      this.yRatio,
      this.widthRatio,
      this.heightRatio});

  _init() {
    this.x = this.xRatio * this.image.width;
    this.y = this.yRatio * this.image.height;
    this.width = this.widthRatio * this.image.width;
    this.height = this.heightRatio * this.image.height;
  }

  final Paint colorPaint = new Paint()
    ..color = Colors.red.shade500
    ..strokeWidth = 5.0
    ..style = PaintingStyle.stroke;

  @override
  void paint(Canvas canvas, Size size) {
    print("Preparing Paint");
    _init();
    print("H: ${this.image.height} | W: ${this.image.width}");
    print("CH: ${size.height} | CW: ${size.width}");
    canvas.scale(0.5);
    canvas.drawImage(this.image, Offset(0, 0), Paint());
    canvas.drawRect(Rect.fromLTWH(x, y, width, height), colorPaint);
    if (image != null) {
      TextSpan span = new TextSpan(
          style: new TextStyle(color: Colors.red[500], fontSize: 20),
          text: name);
      TextPainter tp = new TextPainter(
          text: span,
          textAlign: TextAlign.left,
          textDirection: TextDirection.ltr);
      tp.layout();
      tp.paint(canvas, new Offset(x + 10, y + 5));
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
