import 'package:ase_camera_test/camera.dart';
//import 'package:ase_camera_test/webcam.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ASE FRATS Camera Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'ASE Frats Camera Demo'),
    );
  }
}

class MyHomePage extends StatelessWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  void _launchCamera(BuildContext context) {
    // Launch Camera
    //Scaffold.of(context).showSnackBar(SnackBar(content: Text("Launching Le Camera Wei"),));
    if (kIsWeb) {
      // Web (Enable for web support)
      //Navigator.push(context, MaterialPageRoute(builder: (context) => WebCamApp()),);
    } else {
      // Mobile UI
      Navigator.push(
        context, MaterialPageRoute(builder: (context) => CameraApp()),);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Click the camera to launch the camera',
            ),
          ],
        ),
      ),
      floatingActionButton: Builder(
        builder: (context) => FloatingActionButton(
          onPressed: () => _launchCamera(context),
          tooltip: 'Launch Face Recognition',
          child: Icon(Icons.camera_alt),
        ),
      ),
    );
  }
}
